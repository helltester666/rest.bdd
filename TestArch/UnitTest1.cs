using NUnit.Framework;

namespace TestArch
{
    public class Tests : TestBase
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [Property("feature", "highrate")]
        public void HighRateTest()
        {
            I.Have.Order.Standart();
            I.Have.Client.WithCurrentOrder();
            I.Have.Order.GetInfo();

            I.Am.Client.WithLocation(1.5, 2.5);
            I.Am.Client.GetInfo();

            I.Have.Order.WithLocation(10, 10);
            I.Am.Client.GetInfo();

            I.Am.Driver.SendDriverRequest();
            I.Am.Driver.GetInfo();
        }

        [Test]
        [Property("feature", "softprotect")]
        public void SoftProtectTest()
        {
            I.Am.Client.WithLocation(5, 10);
            I.Am.Client.GetInfo();

            I.Am.Driver.SendDriverRequest();
            I.Am.Driver.GetInfo();
        }
    }
}