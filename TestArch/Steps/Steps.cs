﻿using TestArch.TestContext;

namespace TestArch.Steps
{
    public class Steps
    {
        public Actor Actor;
        public Object Objects;

        public ActorSteps Am;
        public ObjectSteps Have;

        public Steps()
        {
            Actor = new Actor();
            Objects = new Object();

            Am = new ActorSteps(Actor, Objects);
            Have = new ObjectSteps(Actor, Objects);
        }
    }
}
