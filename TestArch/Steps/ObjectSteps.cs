﻿using TestArch.TestContext;
using Object = TestArch.TestContext.Object;

namespace TestArch.Steps
{
    public class ObjectSteps
    {
        public OrderSteps Order;
        public ClientObjectsSteps Client;

        public ObjectSteps(Actor actor, Object objects)
        {
            Order = new OrderSteps(actor, objects);
            Client = new ClientObjectsSteps(actor, objects);
        }
    }
}
