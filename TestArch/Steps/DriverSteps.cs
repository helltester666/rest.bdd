﻿using System;
using TestArch.TestContext;
using Object = TestArch.TestContext.Object;

namespace TestArch.Steps
{
    public class DriverSteps
    {
        private readonly Actor _actors;
        private readonly Object _objects;

        public DriverSteps(Actor actors, Object objects)
        {
            _actors = actors;
            _objects = objects;
        }

        public void SendDriverRequest()
        {
            Console.WriteLine("Driver has sent a request to order");
        }

        public void GetInfo()
        {
            Console.WriteLine(_actors.Driver.ToString());
        }
    }
}
