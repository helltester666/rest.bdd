﻿using Models.Domain;
using System;
using TestArch.TestContext;
using Object = TestArch.TestContext.Object;

namespace TestArch.Steps
{
    public class OrderSteps
    {
        private readonly Object _object;

        public OrderSteps(Actor actor, Object @object)
        {
            _object = @object;
        }

        public void Standart()
        {
            _object.Order = new Order();
        }

        public void GetInfo()
        {
            Console.WriteLine(_object.Order);
        }

        public void WithLocation(double x, double y)
        {
            _object.Order.From.Longitude = x;
            _object.Order.From.Latitude = y;

            Console.WriteLine($"Order with coordinates created: {x} {y}");
        }
    }
}
