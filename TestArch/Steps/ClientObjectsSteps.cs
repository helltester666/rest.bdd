﻿using TestArch.TestContext;

namespace TestArch.Steps
{
    public class ClientObjectsSteps
    {
        private readonly Actor _actor;
        private readonly Object _object;

        public ClientObjectsSteps(Actor actor, Object @object)
        {
            _actor = actor;
            _object = @object;
        }

        public void WithCurrentOrder()
        {
            _actor.Client.Order = _object.Order;
        }
    }
}
