﻿using TestArch.TestContext;

namespace TestArch.Steps
{
    public class ActorSteps
    {
        public ClientSteps Client;
        public DriverSteps Driver;

        public ActorSteps(Actor actors, Object objects)
        {
            Client = new ClientSteps(actors, objects);
            Driver = new DriverSteps(actors, objects);
        }
    }
}
