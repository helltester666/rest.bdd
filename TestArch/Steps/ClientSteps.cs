﻿using System;
using TestArch.TestContext;
using Object = TestArch.TestContext.Object;

namespace TestArch.Steps
{
    public class ClientSteps
    {
        private readonly Actor _actors;
        private readonly Object _objects;
        public ClientSteps(Actor actors, Object objects)
        {
            _actors = actors;
            _objects = objects;
        }

        public void WithLocation(double x, double y)
        {
            _actors.Client.Location.Latitude = x;
            _actors.Client.Location.Longitude = y;

            Console.WriteLine("Location was set");
        }

        public void GetInfo()
        {
            Console.WriteLine(_actors.Client.ToString());
        }
    }
}
