﻿using NUnit.Framework;
using System;
using TestArch.TestContext;

namespace TestArch
{
    [TestFixture]
    public class TestBase
    {
        protected Steps.Steps I;

        private ITestConfiguration _config;

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Defining configuration for test");

            I = new Steps.Steps();
            GetRequiredConfiguration();

            Console.WriteLine($"Configuration loaded: {_config.ToString()}");
        }

        private void GetRequiredConfiguration()
        {
            var featureName = NUnit.Framework.TestContext.CurrentContext.Test.Properties.Get("feature").ToString();

            if (featureName == "highrate")
            {
                _config = new HighRateConfiguration(I.Actor, I.Objects);
            }
            else
            {
                _config = new SoftProtectConfig(I.Actor, I.Objects);
            }
        }

        [TearDown]
        public void TearDown()
        {
            _config.ClearContext();
            I.Am.Driver.GetInfo();
            I.Am.Client.GetInfo();
        }
    }
}
