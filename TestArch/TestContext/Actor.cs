﻿using Models.Domain;

namespace TestArch.TestContext
{
    public class Actor
    {
        public Driver Driver;
        public Client Client;
    }
}
