﻿using Models.Domain;

namespace TestArch.TestContext
{
    public class HighRateConfiguration : ITestConfiguration
    {
        private readonly Actor _actors;
        private readonly Object _objects;

        public HighRateConfiguration(Actor actors, Object objects)
        {
            _actors = actors;
            _objects = objects;
            Configure();
        }

        public override string ToString()
        {
            return "Highrate test configuration";
        }

        public void Configure()
        {
            _actors.Driver = new Driver
            {
                FirstName = "Highrate",
                LastName = "driver",
                Phone = "+7999 1234567",
                Location = new Location
                {
                    Latitude = 1,
                    Longitude = 2
                }
            };

            _actors.Client = new Client
            {
                LastName = "Highrate",
                FirstName = "client",
                Phone = "+7888 7774455",
                Location = new Location
                {
                    Longitude = 3,
                    Latitude = 4
                }
            };
        }

        public void ClearContext()
        {
            _actors.Driver = new Driver();
            _actors.Client = new Client();
            _objects.Order = new Order();
        }
    }
}
