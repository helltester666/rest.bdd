﻿using Models.Domain;

namespace TestArch.TestContext
{
    public class SoftProtectConfig : ITestConfiguration
    {
        private readonly Actor _actors;
        private readonly Object _objects;

        public SoftProtectConfig(Actor actors, Object objects)
        {
            _actors = actors;
            _objects = objects;
            Configure();
        }

        public override string ToString()
        {
            return "Softprotect test configuration";
        }

        public void Configure()
        {
            _actors.Driver = new Driver
            {
                FirstName = "SoftProtect",
                LastName = "driver",
                Phone = "+7111 2223344",
                Location = new Location
                {
                    Latitude = 2,
                    Longitude = 3
                }
            };

            _actors.Client = new Client
            {
                LastName = "Softprotect",
                FirstName = "client",
                Phone = "+7222 3339900",
                Location = new Location
                {
                    Longitude = 3,
                    Latitude = 2
                }
            };
        }

        public void ClearContext()
        {
            _actors.Driver = new Driver();
            _actors.Client = new Client();
            _objects.Order = new Order();
        }
    }
}
