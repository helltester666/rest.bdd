﻿namespace TestArch.TestContext
{
    public interface ITestConfiguration
    {
        void Configure();
        void ClearContext();
    }
}
