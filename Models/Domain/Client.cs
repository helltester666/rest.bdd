﻿namespace Models.Domain
{
    public class Client
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public Location Location { get; set; }
        public Order Order { get; set; }

        public Client()
        {
            FirstName = "New";
            LastName = "New";
            Phone = "New";
            Order = new Order();
            Location = new Location
            {
                Longitude = 0,
                Latitude = 0
            };
        }

        public override string ToString()
        {
            return
                $"FullName: {FirstName} {LastName}" +
                $", Phone: {Phone}" +
                $", Location: {Location.Latitude} {Location.Longitude}" +
                $", Order: {Order}";
        }
    }
}
