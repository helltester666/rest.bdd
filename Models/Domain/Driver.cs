﻿namespace Models.Domain
{
    public class Driver
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public Location Location { get; set; }

        public Driver()
        {
            FirstName = "New";
            LastName = "New";
            Phone = "New";
            Location = new Location
            {
                Longitude = 0,
                Latitude = 0
            };
        }

        public override string ToString()
        {
            return
                $"FullName: {FirstName} {LastName}" +
                $", Phone: {Phone}" +
                $", Location: {Location.Latitude} {Location.Longitude}";
        }
    }
}
