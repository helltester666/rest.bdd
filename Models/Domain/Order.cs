﻿namespace Models.Domain
{
    public class Order
    {
        public string Name { get; set; }
        public Location From { get; set; }
        public Location To { get; set; }

        public Order()
        {
            Name = "New";
            From = new Location
            {
                Longitude = 20,
                Latitude = 20
            };

            To = new Location
            {
                Longitude = 0,
                Latitude = 0
            };
        }

        public override string ToString()
        {
            return $"Name: {Name}" +
                   $", Location from: {From.Longitude} {From.Latitude}" +
                   $", Location to: {To.Longitude} {To.Latitude}";
        }
    }
}
